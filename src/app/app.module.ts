import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TableComponent} from './table/table.component';
import {FormsModule} from "@angular/forms";
import {ApiService} from "./global/services/api/api.service";
import {HttpModule} from "@angular/http";
import { MyDatePipe } from './global/pipes/my-date/my-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    MyDatePipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
