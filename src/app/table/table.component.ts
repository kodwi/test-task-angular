import {Component, Input, OnInit} from '@angular/core';
import {IResponse} from "../global/interfaces";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  /*
   * Данные для отображения
   * */
  @Input() data: IResponse[];
  /*
  * Мапа <код_типа> => <читабельный_формат>
  * */
  @Input() searchTypesMap: Map<string, string>;

  constructor() {
  }

  ngOnInit() {
  }

}
