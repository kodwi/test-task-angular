import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import {IResponse} from "../../interfaces";

/*
* API для получения данных с сервера
* */

@Injectable()
export class ApiService {
  private readonly _apiUrl: string;

  constructor(private _http: Http) {
    this._apiUrl = 'http://dev2.poiskznakov.ru/api-rest/test-me/';
  }

  getResult(searchType: string): Observable<IResponse[]> {
    const params = {};

    if (searchType !== 'all') {
      params['stype'] = searchType;
    }

    return this._http
      .get(this._apiUrl, {params: params})
      .map(result => result.json());
  }
}
