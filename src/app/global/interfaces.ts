export interface IResponse {
  search_type: string;
  search_string: string;
  search_start: string;
  found: number;
}
