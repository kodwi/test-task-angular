import {Pipe, PipeTransform} from '@angular/core';

/*
 * Пайп для конвертирования даты конкретно нашего случая DD.MM.YYYY => YYYY-MM-DD
 * */

@Pipe({
  name: 'myDate'
})
export class MyDatePipe implements PipeTransform {
  transform(value: string): string {
    const separated = value.split('.');

    const day = separated[0];
    const month = separated[1];
    const year = separated[2];

    return `${year}-${month}-${day}`;
  }
}
