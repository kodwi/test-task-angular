import {Component, OnInit} from '@angular/core';
import {ApiService} from "./global/services/api/api.service";
import {IResponse} from "./global/interfaces";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  /*
   * Коды типов поиска
   * */
  searchTypes: string[];
  /*
   * Мапа <код_типа> => <читабельный_формат>
   * */
  searchTypesMap: Map<string, string>;
  /*
   * Выбранный код типа
   * */
  currentSearchType: string;
  /*
   * Данные для отображения в таблице
   * */
  data: IResponse[];

  /*
   * Инжектим API для получения данных
   * */
  constructor(private _apiService: ApiService) {
  }

  /*
   * После инициализации компонента
   * */
  ngOnInit() {
    /*
     * Заполняем список кодов типов и заодно мапу
     * */
    this._customPush('all', 'Все');
    this._customPush('wdesc', 'По словесному элементу');
    this._customPush('owner', 'По владельцу');
    this._customPush('document_number', 'По номеру документа');

    /*
     * Устанавливаем текущий тип поиска из локального хранилища
     * или первый тип из списка (all), если там еще пусто.
     * */
    this.currentSearchType = localStorage.getItem('searchType') || this.searchTypes[0];

    /*
     * Обновляем данные
     * */
    this.refreshData();
  }

  /*
   * Меняем тип поиска, попутно записывая его в локальное хранилище и обновляя данные
   * */
  changeSearchType(type: string): void {
    localStorage.setItem('searchType', this.currentSearchType = type);

    this.refreshData();
  }

  /*
   * Обновление данных с помощью API сервиса
   * */
  refreshData(): void {
    /*
     * Ставим data в null, чтобы показался текст "Загрузка"
     * */
    this.data = null;

    /*
     * Вызываем API, он шлет запрос, мы подписываемся на результат,
     * и как ответ приходит, смотрим:
     * если все ок, то data = result,
     * если ошибка, то data = undefined + в консоль соответствующий текст
     * */
    this._apiService.getResult(this.currentSearchType).subscribe(
      result => this.data = result,
      () => {
        this.data = undefined;

        console.error("Couldn't get the data from the server");
      }
    );
  }

  /*
   * Хелпер-функция, чтобы одновременно заполнять данными и список типов, и мапу
   * */
  private _customPush(key: string, value: string): void {
    this.searchTypes = this.searchTypes || [];
    this.searchTypesMap = this.searchTypesMap || new Map();

    this.searchTypes.push(key);
    this.searchTypesMap.set(key, value);
  }
}
