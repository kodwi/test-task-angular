import { TestTaskAngularPage } from './app.po';

describe('test-task-angular App', () => {
  let page: TestTaskAngularPage;

  beforeEach(() => {
    page = new TestTaskAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
